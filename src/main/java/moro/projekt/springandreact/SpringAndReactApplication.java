package moro.projekt.springandreact;

import java.util.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.context.support.*;
import ma.glasnost.orika.*;
import ma.glasnost.orika.impl.*;
import moro.projekt.springandreact.entity.*;
import moro.projekt.springandreact.repository.*;
import moro.projekt.springandreact.service.*;



@SpringBootApplication
public class SpringAndReactApplication  implements CommandLineRunner {

	// Implementing CommandLineRunner helps to execute the repository methods at start of the application.

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UserServiceImpl userService;


	@Autowired
	UserRepository userRepository;



	@Bean
	public MapperFacade mapperFacade() {
		MapperFactory mapperFactory
				= new DefaultMapperFactory.Builder().build();
		return mapperFactory.getMapperFacade();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringAndReactApplication.class, args);
	}



	//testing testing testing

	@Override
	public void run(String... args) throws Exception {
		//Code to run at application startup




//		UserDto u = new UserDto();
//		u.setId(new Long(78));
//		u.setName("Matěj");
//		u.setSurname("Kovář");
//		u.setAge(27);

//		userService.getUserById(10);
//		userService.addUser(u);
//		userService.editUser(u);
//		userService.deleteUserById(new Long(3));
//
//		List <UserDto> lst = userService.getUserList();
//
//		for(UserDto us : lst) {
//			logger.info("id " + us.getId() + "name " + us.getName() + "surname " + us.getSurname() );
//
//		}
	}
}

// start production build:

//otevrit slozku s projektem
// mvn clean install
// java -jar target/spring-and-react-0.0.1-SNAPSHOT.jar    (spring-and-react je nazev projektu)


// start react:

// otevrit slozku fronted
// npm start


