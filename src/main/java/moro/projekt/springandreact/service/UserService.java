package moro.projekt.springandreact.service;

import java.util.List;
import moro.projekt.springandreact.entity.*;

public interface UserService {
    UserDto addUser(UserDto user);
    List<UserDto> getUserList();
    UserDto getUserById(long idUser);
    void deleteUserById(long idUser);
    UserDto editUser(UserDto user);
}
