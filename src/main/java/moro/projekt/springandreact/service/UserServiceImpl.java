package moro.projekt.springandreact.service;

import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.*;
import moro.projekt.springandreact.entity.*;
import moro.projekt.springandreact.exception.*;
import moro.projekt.springandreact.repository.*;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private MapperFacade mapperFacade;


    @Transactional
    @Override
    public UserDto addUser(UserDto user) {

        log.info("Try add user " + user);
        if (user.getId() == null) {
            log.info("User " + user.getSurname() + " was added.");
            return this.saveUserEntity(user);
        }

        try {
            this.getUserById(user.getId());
            log.info("User " + user.getSurname() + " was added.");

        } catch (ItemNotFoundException ex) {
            return this.saveUserEntity(user);
        }
        log.info("User with id "+ user.getId() + " is in DB and was not added.");
        return user;

//        return this.saveUserEntity(user);

    }

    @Transactional(readOnly = true)
    @Override
    public List<UserDto> getUserList() {
        log.info("List of users");
        List<UserEntity> userEntityLst = userRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        List<UserDto> userDtoLst = mapperFacade.mapAsList(userEntityLst, UserDto.class);
        return userDtoLst;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDto getUserById(long idUser) {

        Optional optUser = userRepository.findById(idUser);
        if(!(optUser.isPresent())){
            throw new ItemNotFoundException("Item with id " + idUser + " not found!");
        }
        log.info("Get user with id " + idUser);
        return mapperFacade.map(
                optUser.get(),
                UserDto.class
        );
    }

    @Override
    public void deleteUserById(long idUser) {
        log.info("Delete user with id " + idUser);
        try {
            userRepository.deleteById(idUser);
        } catch (EmptyResultDataAccessException ex) {
            throw  new ItemNotFoundException("Item with id " + idUser + " not found !!!");

        }
    }

    @Transactional
    @Override
    public UserDto editUser(UserDto user) {
        log.info("Try edit user");
        if(user.getId() == null) {
            throw new IdUserNotFoundException("Cannot edit user, id is null");
        }
        //      zkousim, zda uzivatel daneho id je v DB
        this.getUserById(user.getId());
        log.info("UserEntity" + user.getId() + "was edited");
        return this.saveUserEntity(user);
    }

    public UserDto saveUserEntity(UserDto userDto){
        return mapperFacade.map(userRepository.save(
                mapperFacade.map(userDto, UserEntity.class)),
                UserDto.class
        );
    }
}
