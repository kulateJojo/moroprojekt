package moro.projekt.springandreact.entity;

import javax.persistence.*;
import lombok.Data;

/**
 * objekt poskytujici pristup do databaze
 */


@Data
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "age")
    private Integer age;
}
