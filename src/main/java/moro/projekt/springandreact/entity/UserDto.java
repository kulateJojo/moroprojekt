package moro.projekt.springandreact.entity;

import javax.validation.constraints.*;
import lombok.Data;

/**
 * slouzi k prenosu dat z formularu na webu (EditUser a AddUser)
 */

@Data  //POJO objekt  - getter, setter, requiredAllArgCon, eqaul+hashCode, toString
public class UserDto {

    private Long id;
    @Size(min = 2, max = 40, message = "Zadejte jméno v rozmezí 2-40 znaků")
    private String name;
    @Size(min = 2, max = 40, message = "Zadejte jméno v rozmezí 2-40 znaků")
    private String surname;
    @NotNull(message = "Pole musí být vyplněné")
    @Digits(integer = 3, fraction = 0, message = "Můžete mít maximálně 999 let.")
    @PositiveOrZero(message = "Číslo musí být kladné nebo nula.")
    private Integer age;
}
