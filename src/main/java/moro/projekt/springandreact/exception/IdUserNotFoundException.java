package moro.projekt.springandreact.exception;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class
IdUserNotFoundException extends RuntimeException {

    public IdUserNotFoundException(String message) {
        super(message);
    }
}
