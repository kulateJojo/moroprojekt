package moro.projekt.springandreact;

import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;
import moro.projekt.springandreact.entity.*;
import moro.projekt.springandreact.service.*;

@Service("customUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDto userDto = userService.getUserById(new Long(55));
        return new MyUserPrincipal(userDto);

    }


    private List<GrantedAuthority> getGrantedAuthorities() {
        List<GrantedAuthority> authoritiesLst = new ArrayList();
        authoritiesLst.add(new SimpleGrantedAuthority("USER"));
        return authoritiesLst;



    }
}
