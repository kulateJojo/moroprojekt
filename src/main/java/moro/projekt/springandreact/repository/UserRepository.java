package moro.projekt.springandreact.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import moro.projekt.springandreact.entity.*;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

}
