package moro.projekt.springandreact.controller;

import java.util.List;
import javax.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.*;
import lombok.extern.slf4j.*;
import moro.projekt.springandreact.entity.*;
import moro.projekt.springandreact.service.UserServiceImpl;


@Slf4j
@RequestMapping("/user")
@RestController
public class Controller {

    @Autowired
    private UserServiceImpl userService;


    
    /**
     * ziskani uzivatele z DB podle id
     * dostupne na /user/id - kde id je id uzivatele
     * @param id id hledaneho uzivatele
     * @return hledany uzivatel
     */
    @GetMapping("/{id}")
    public UserDto getById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    /**
     * pridani noveho uzivatele
     * dostupne na /user
     * @param userDto novy uzivatel
     * @param bindingResult chyby atributu noveho uzivatele
     * @return uzivatel, ktereho chci pridat
     */
    @PostMapping
    public UserDto add(
            @Valid @RequestBody UserDto userDto, BindingResult bindingResult
    ) {
        log.info("Try add user POST");
        if (bindingResult.hasErrors()) {
            log.info("Validation error");
            return userDto;
        }
        log.info("Request body OK");
        return userService.addUser(userDto);
    }

    @PutMapping("/{id}")
    public UserDto edit(@Valid @RequestBody UserDto userDto,
                     BindingResult bindingResult,
                     @PathVariable int id) {
        log.info("Try edit user put");
        if (bindingResult.hasErrors()) {
            log.info("Validation error");
            return userDto;
        }
        log.info("Request body OK");
        return userService.editUser(userDto);
    }

    @GetMapping
    public List<UserDto> userDtoLst() {
        return userService.getUserList();
    }

    @DeleteMapping("/secured/delete/{id}")
    public String delete(@PathVariable long id
//                         ,@RequestHeader("User") String user
    ) {
//        log.info("delete" + user);
        log.info("jsem v mazani");
//        userService.deleteUserById(id);
        return "Delete done";
    }
}






