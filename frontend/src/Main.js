import React, {Component} from 'react';
import Table from './Table.js';
import AddUser from './AddUser.js';
import ReactDOM from 'react-dom';
import EditUser from './EditUser.js';
import request from 'superagent';
import callback from 'superagent';

const required = fieldValue => fieldValue ? undefined : "Please enter a value";
const lessThanValue = value => fieldValue =>
        fieldValue < value ? undefined : `Value must be less than ${value}`;

class Main extends Component {

    constructor() {
        super();
        this.state = {
            users: [
                {id: 1, name: 'Jáchym', surname: 'Vyskočil', age: '25'},
                {id: 2, name: 'Denis', surname: 'Bureš', age: '32'},
                {id: 3, name: 'Kristián', surname: 'Pavelka', age: '21'}
            ],
            header: ["Id", "Jméno", "Příjmení", "Věk", ""
            ],
            validations:{
                price:["totalPrice"],
                        quantity: [required,lessThanValue(100)],
            },
            error:{}
        };
        this.onRowDeleted = this.onRowDeleted.bind(this);
        this.addUser = this.addUser.bind(this);
        this.onRowEdit = this.onRowEdit.bind(this);
        this.editUser = this.editUser.bind(this);
        this.editUserCanceled = this.editUserCanceled.bind(this);
        this.setUsers = this.setUsers.bind(this);
        this.getUserLstFromDB = this.getUserLstFromDB.bind(this);
        // this.validateField = this.validateField.bind(this);
    }


    
    render() {
        return (
                <div>
                    <Table users={this.state.users}
                           header={this.state.header}
                           onRowDeleted={this.onRowDeleted}
                           onRowEdit={this.onRowEdit}
                    />

                    <div>
                        <AddUser addUser={this.addUser} validateField={this.validateField}/>
                        {/*<h3>Uživatelé ({this.state.users.length})</h3>*/}
                    </div>

                    <div id="editUser"></div>

                </div>
        );
    }

    onRowDeleted(id) {
        console.log(`remove ${id}`);
        request.delete('/user/secured/delete/'.concat(id))
                .set("User","karel")
                .end(() => {
                    // TODO udelat lepe cekani na aktualizaci DB
                    function sleep(milliseconds) {
                        var start = new Date().getTime();
                        for (var i = 0; i < 1e7; i++) {
                            if ((new Date().getTime() - start) > milliseconds) {
                                break;
                            }
                        }
                    }

                    sleep(500);
                    this.getUserLstFromDB();
                })

    }

    addUser(user) {
        console.log(`add ${user.surname}`);

        request.post('/user')
                .set('Content-Type', 'application/json')
                .send(user)
                .end(() => {
                    // TODO udelat lepe cekani na aktualizaci DB
                    function sleep(milliseconds) {
                        var start = new Date().getTime();
                        for (var i = 0; i < 1e7; i++) {
                            if ((new Date().getTime() - start) > milliseconds) {
                                break;
                            }
                        }
                    }

                    sleep(500);
                    this.getUserLstFromDB();
                })
    }

    setUsers(userLst) {
        console.log(`set ${userLst[userLst.length - 1].name}`);
        this.setState({
            users: userLst
        });
    }

    // zobrazeni formulare pro editaci vybraneho uzivatele
    onRowEdit(user) {
        console.log(`edit row ${user.name}`);

        // DULEZITE bylo nastaveni key, az pak zacalo fungovat prekresleni obsahu pri kliknuti ne Editovat u jineho radku
        // viz https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
        {ReactDOM.render(<EditUser editUser = {this.editUser}
                                   editUserCanceled = {this.editUserCanceled}
                                   user = {user}
                                   key = {user.id}
                                   validateField = {this.validateField}
                />,
                document.getElementById('editUser'));}
    }

    editUser(user) {

        console.log('vypisuju si: /user/'.concat(user.id));

        request.put('/user/'.concat(user.id))
                .set('Content-Type', 'application/json')
                .send(user)
                .end(() => {
                    // TODO udelat lepe cekani na aktualizaci DB
                    function sleep(milliseconds) {
                        var start = new Date().getTime();
                        for (var i = 0; i < 1e7; i++) {
                            if ((new Date().getTime() - start) > milliseconds) {
                                break;
                            }
                        }
                    }

                    sleep(500);
                    this.getUserLstFromDB();
                })

    }

    editUserCanceled() {
        {ReactDOM.render(<div id="editUser"></div>, document.getElementById('editUser'));}
    }

    // naplneni state.users z databaze

    getUserLstFromDB() {
        request.get('/user')
                .type('application/json')
                .accept('json')
                .then(res => {
                    this.setUsers(res.body);
                }).catch(err => {
            console.log(err.message + err.response);
        })
    }

    componentDidMount() {
        this.getUserLstFromDB();

    }

    // validateField (fieldName,fieldValue) {
    //     let errorMessage;
    //     this.state.validations[fieldName].forEach( (validation)=>{
    //         console.log(`validuji ${fieldName} validaci ${validation}`);
    //
    //         if(typeof validation==="function"){
    //             console.log(`jde o funkci`);
    //
    //             errorMessage = validation(fieldValue,this.state.user);
    //             this.setState(
    //                     function (prevState, props) {
    //                         return {error:{
    //                             ...this.state.error,
    //                             [fieldName]:errorMessage
    //                         }
    //                         }
    //                     }
    //             )
    //             // this.setState({
    //             //     error:{
    //             //         ...this.state.error,
    //             //         [fieldName]:errorMessage
    //             //     }
    //             // })
    //             if(errorMessage){return}
    //         } else {
    //             console.log(`${validation} NENI funkce`);
    //
    //             this.validateField(validation,this.state.user[validation]);
    //         }
    //     });
    // }

}

// VALIDACE
// inspirovano https://blog.cloudboost.io/how-i-did-validation-for-forms-in-react-js-5e2784803d7b
function validateField (fieldName,fieldValue) {
    let errorMessage;
    this.state.validations[fieldName].forEach( (validation)=>{
        console.log(`validuji ${fieldName} validaci ${validation}`);

        if(typeof validation==="function"){
            console.log(`jde o funkci`);

            errorMessage = validation(fieldValue,this.state.user);
            // pokud neprojde kontrolou, ulozim do state chybovou hlasku, ktera se tak zobrazi u daneho inputu
            // pokud je ok, tak pripadnou chybovou hlasku smazu
            this.setState(
                function () {
                    let error = this.state.error;
                    error[fieldName] = errorMessage
                    return {error: error}
                }
            )
            if(errorMessage){return}
        } else {
            console.log(`${validation} NENI funkce`);

            this.validateField(validation,this.state.user[validation]);
        }
    });
}

// v pripade chyby vracet text k vypsani jinak nic
const stringLength = (minValue, maxValue) => fieldValue => {
    if (fieldValue == null || (fieldValue.length > maxValue) || (fieldValue.length < minValue)) {
        console.log("SPATNA DELKA STRINGU")
        return `Delka musi byt mezi ${minValue} a ${maxValue} znaky`;
    } else {
        return undefined;
    }
}
const numberBetween = (minValue, maxValue) => fieldValue => {
    if (fieldValue == null || (fieldValue > maxValue) || (fieldValue < minValue)) {
        console.log("SPATNY CISELNY ROZSAH")

        return `Hodnota musi byt mezi ${minValue} a ${maxValue}`;
    } else {
        return undefined;
    }
}

export {validateField, stringLength, numberBetween};
export default Main;




