import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Table extends Component {
    render() {
        return (
                <div>
                    <table>
                        <tbody>
                            <Header header={this.props.header}/>

                            {
                                this.props.users.map(user => {
                                return (<Row user={user}
                                             key={user.id}
                                             onRowDeleted={this.props.onRowDeleted}
                                             onRowEdit={this.props.onRowEdit}
                                    />)
                                })
                            }
                        </tbody>
                    </table>
                </div>
        );
    }
}

class Row extends Component {
    render() {
        return (
                <tr>
                    {/*vykresli bunky s hodnotami konkretniho uzivatele*/}
                    {
                        Object.values(this.props.user).map((value) => {
                            return (<Cell text={value}
                                          key={this.props.user.id}
                            />)
                        })
                    }

                    <td>
                        <button onClick={() => this.props.onRowDeleted(this.props.user.id)}>
                            Smazat
                        </button>
                        <button onClick={() => this.props.onRowEdit(this.props.user)}>
                            Editovat
                        </button>
                    </td>
                </tr>
        );
    }
}

class Cell extends Component {
    render() {
        return (
                <td>{this.props.text}</td>
        );
    }
}

class Header extends Component {
    render() {
        return (
                <tr>
                    {
                        this.props.header.map((columnName) => {
                            return (<th>{columnName}</th>)
                        })
                    }
                </tr>
        );
    }
}





Table.propTypes = {
    users: PropTypes.array.isRequired,
    header: PropTypes.array.isRequired,
    onRowDeleted: PropTypes.func.isRequired,
    onRowEdit: PropTypes.func.isRequired
};

Row.propTypes = {
    user: PropTypes.object.isRequired,
    onRowDeleted: PropTypes.func.isRequired,
    onRowEdit: PropTypes.func.isRequired
};

Cell.propTypes = {
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

Header.propTypes = {
    user: PropTypes.array.isRequired
};