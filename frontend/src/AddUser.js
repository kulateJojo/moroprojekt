import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {validateField, stringLength, numberBetween} from "./Main"


export default class AddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                name: "" ,
                surname: "",
                age: ""
            }
            ,
            validations:{
                name: [stringLength(2, 40)],
                surname: [stringLength(2, 40)],
                age: [numberBetween(1,120)]
            }
            ,
            error:{}
        };
        this.handleChange = this.handleChange.bind(this);
        this.validateField = validateField.bind(this);

    }

    render() {
        return (
        <div>
                <h3>Přidej uživatele</h3>
                <form onSubmit={(e) => this.handleSubmit(e)} ref="addUser">

                    <div>Jméno:</div>
                    <input id="name" type="text" name="name" ref="name" value={this.state.user.name} onChange={(e)=> this.handleChange(e.target.value,"name")} /><br />
                    <span className="error-message">{this.state.error.name}</span>

                    <div>Příjmení:</div>
                    <input id="surname" type="text" name="surname" ref="surname" value={this.state.user.surname} onChange={(e)=> this.handleChange(e.target.value,"surname")}/><br />
                    <span className="error-message">{this.state.error.surname}</span>

                    <div>Věk:</div>
                    <input id="age" type="number" name="age" ref="age" value={this.state.user.age} onChange={(e)=> this.handleChange(e.target.value,"age")}/><br />
                    <span className="error-message">{this.state.error.age}</span>

                    <div>
                    <input type="submit" value="Přidat" />
                    </div>

                </form>
            </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();

        //TODO udelat lepe
        // volam duplicitne pri handleChange i submitu, aby se nestalo, ze uzivatel odesle nevalidni formular (nektera hodnota bude prazdna napr)
        this.handleChange(this.state.user.name,"name");
        this.handleChange(this.state.user.surname,"surname");
        this.handleChange(this.state.user.age,"age");

        console.log("error" + this.state.error.name);

        if (this.state.error.name || this.state.error.surname || this.state.error.age) {
            console.log("byla chyba")
        } else {
            this.props.addUser({
                id: "",
                name: this.state.user.name,
                surname: this.state.user.surname,
                age: this.state.user.age
            });
            
            // vynuluji formular
            this.setState({user: {}})
            ReactDOM.findDOMNode(this.refs.addUser).reset();
        }
    }

    handleChange(fieldValue, fieldName) {
        let user = this.state.user
        user[fieldName] = fieldValue;

        this.setState(
            function () {
                if(this.state.validations[fieldName]){
                    // pro dany input existuji nejake validace k provedeni
                    if (this.validateField(fieldName, fieldValue)) {
                        return {user: user};
                    }
                }
            }
        );
    }
}

AddUser.propTypes = {
    addUser: PropTypes.func.isRequired
};
