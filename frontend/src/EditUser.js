import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {validateField, stringLength, numberBetween} from "./Main"

export default class EditUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
            ,
            validations:{
                name: [stringLength(2, 40)],
                surname: [stringLength(2, 40)],
                age: [numberBetween(1, 120)]
            }
            ,
            error:{}
        };
        this.handleChange = this.handleChange.bind(this);
        this.validateField = validateField.bind(this);
    }
    
    render() {
        console.log(`vykresluji editaci pro ${this.state.user.name}`);

        return (
                <div className="editUser">
                    <h3>Edituj uživatele</h3>
                    <form onSubmit={(e) => this.handleSubmit(e)} ref="editUser">

                        <div>Jméno:</div>
                        <input id="name" type="text" name="name" ref="name" value={this.state.user.name} onChange={(e)=> this.handleChange(e.target.value,"name")}/><br />
                        <span className="error-message">{this.state.error.name}</span>

                        <div>Příjmení:</div>
                        <input id="surname" type="text" name="surname" ref="surname" value={this.state.user.surname} onChange={(e)=> this.handleChange(e.target.value,"surname")}/><br />
                        <span className="error-message">{this.state.error.surname}</span>

                        <div>Věk:</div>
                        <input id="age" type="number" name="age" ref="age" value={this.state.user.age} onChange={(e)=> this.handleChange(e.target.value,"age")}/><br />
                        <span className="error-message">{this.state.error.age}</span>

                        <div>
                        <input type="submit" value="Uložit změny" />
                        <button onClick={() => this.props.editUserCanceled()}>Zruš změny</button>
                        </div>
                    </form>
                </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.error.name || this.state.error.surname || this.state.error.age) {
            console.log("byla chyba")
        } else {
            this.props.editUser(this.state.user)

            // nahrazeni html pro editaci uzivatele za puvodni prazdny div
            {ReactDOM.render(<div id="editUser"></div>, document.getElementById('editUser'));}
        }
    }

    handleChange(fieldValue, fieldName) {
        var user = this.state.user
        user[fieldName] = fieldValue;

        this.setState(
            {user: user},
            () => {
                if(this.state.validations[fieldName]){
                    this.validateField(fieldName, fieldValue);
                }
            }
        );
    }
}

EditUser.propTypes = {
    user: PropTypes.object.isRequired,
    editUser: PropTypes.func.isRequired
};

